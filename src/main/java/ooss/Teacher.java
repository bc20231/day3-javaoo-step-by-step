package ooss;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person {
    private List<Klass> klassList = new ArrayList<>();

    public Teacher(int id, String name, int age) {
        super(id, name, age);
    }

    public String introduce() {
        String message = super.introduce() + " I am a teacher.";
        if (this.klassList != null && !this.klassList.isEmpty()) {
            String classListString = this.klassList.stream()
                    .map(klass -> String.valueOf(klass.getNumber()))
                    .collect(Collectors.joining(", "));
            message += String.format(" I teach Class %s.", classListString);
        }
        return message;
    }

    public void assignTo(Klass klass) {
        this.klassList.add(klass);
    }

    public boolean belongsTo(Klass klass) {
        return this.klassList.contains(klass);
    }

    public boolean isTeaching(Student student) {
        return this.klassList.stream()
                .anyMatch(student.getKlass()::equals);
    }
}
