package ooss;

import java.util.Objects;

public class Klass {
    private final int number;
    private Student leader;
    private Person attachedPerson;

    public Klass(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    public void assignLeader(Student student) {
        if (student.isIn(this)) {
            this.leader = student;
            if (this.attachedPerson != null) {
                String role = this.attachedPerson instanceof Teacher ? "teacher" : "student";
                System.out.println(String.format("I am %s, %s of Class %d. I know %s become Leader.",
                        this.attachedPerson.getName(), role, this.number, student.getName()));
            }
        } else
            System.out.println("It is not one of us.");
    }

    public boolean isLeader(Student student) {
        return this.leader != null && this.leader.equals(student);
    }

    public void attach(Person person) {
        this.attachedPerson = person;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }
}
