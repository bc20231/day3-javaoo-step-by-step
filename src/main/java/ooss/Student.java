package ooss;

public class Student extends Person {
    private Klass klass;

    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    public Klass getKlass() {
        return klass;
    }

    public String introduce() {
        String message = super.introduce() + " I am a student.";
        if (this.klass != null) {
            if (this.klass.isLeader(this))
                message += String.format(" I am the leader of class %d.", this.klass.getNumber());
            else
                message += String.format(" I am in class %d.", this.klass.getNumber());
        }
        return message;
    }

    public void join(Klass klass) {
        this.klass = klass;
    }

    public boolean isIn(Klass klass) {
        return this.klass != null && this.klass.equals(klass);
    }
}
